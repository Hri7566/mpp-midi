const fs = require('fs');

class MIDI {
    static load(file) {
        fs.readFile(file, null, (err, data) => {
            const midi = data.toString('hex');

            let readPos = 0x00;
            let headerTag = this.bufferFrom(midi, readPos, 8).toString('ascii');
            if (headerTag !== 'MThd') {
                return console.log("Couldn't find header tag! Is this a MIDI file?");
            }

            readPos = 0x04;
            let headerLength = parseInt(this.bufferFrom(midi, readPos, 4 * 2).toString('hex'), 16);
            
            readPos = 0x08;
            let format = parseInt(this.bufferFrom(midi, readPos, 2).toString('hex'), 16);
            let numTracks = parseInt(this.bufferFrom(midi, readPos + 1, 2).toString('hex'), 16);
            let division = this.padLeft(parseInt(this.bufferFrom(midi, readPos + 1, 2).toString('hex'), 16).toString(2), 16);
            let ticks = 1;
            let ticksPerFrame = false;
            let smpte = -29;

            console.log(`MIDI Name: ${file} | Tracks: ${parseInt(numTracks, 10)} | Format: ${format}`);

            if (division.substr(0, 1) == '0') {
                ticks = parseInt(division.substr(1, 16), 2)
                ticksPerFrame = false;
                console.log(`Ticks per quarter-note: ${ticks}`);
            } else {
                smpte = -parseInt(division.substr(1, 7), 2);
                ticks = parseInt(division.substr(8, 16), 2);
                ticksPerFrame = true;
                console.log(`SMPTE: ${smpte} | Ticks per frame: ${ticks}`);
            }


            if (format !== 0) {
                return console.log("MIDIs other than type 0 aren't supported yet...");
            }
            
            readPos = 0x0E;
            let trackTag = this.bufferFrom(midi, readPos, 8).toString('ascii');
            if (trackTag !== "MTrk") {
                return console.log("Track tag isn't labeled right! Are you sure you know what you're doing?");
            }

            readPos = 0x12;
            let trackLength = parseInt(this.bufferFrom(midi, readPos, 4 * 2).toString('hex'), 16);
            console.log(`Track Length: ${trackLength} bytes`);

            readPos = 0x16;

            for (let i = 0; i < trackLength; i += 0) {
                let pos = readPos + i;
                let dtzerotest = parseInt(this.bufferFrom(midi, pos, 2).toString('hex'), 16) == 0;
                let dt = 0;

                if (dtzerotest) {
                    i++;
                } else {
                    
                }

                let metatest = parseInt(this.bufferFrom(midi, pos, 2).toString('hex'), 16) == 0xFF;
                if (metatest) {
                    i++
                    let metaevt = parseInt(this.bufferFrom(midi, pos, 2).toString('hex'), 16);

                    switch (metaevt) {
                        case 0x00: // sequence number

                            break;
                        case 0x01: // text event

                            break;
                        case 0x02: // copyright notice

                            break;
                        case 0x03: // sequence/track name

                            break;
                        case 0x04: // instrument name

                            break;
                        case 0x05: // lyric

                            break;
                        case 0x06: // marker

                            break;
                        case 0x07: // cue point

                            break;
                        case 0x20: // midi channel prefix

                            break;
                        case 0x2F: // end of track

                            break;
                        case 0x51: // set tempo

                            break;
                        case 0x54: // smpte offset

                            break;
                        case 0x58: // time signature

                            break;
                        case 0x59: // key signature

                            break;
                        case 0x7F: // sequencer-specific meta-event

                            break;
                    }
                }

                i++;
            }
        });
    }

    static bufferFrom(midi, readPos, len) {
        return Buffer.from(midi.substr(readPos * 2, len), 'hex');
    }

    static padLeft(bin, num) {
        let str = "";
        for (let i = 0; i < num; i++) {
            str += "0";
        }
        return str.substr(bin.length) + bin;
    }
}

MIDI.load('Blue_Lampshade.mid');
